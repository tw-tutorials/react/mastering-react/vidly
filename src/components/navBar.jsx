import React from 'react';
import { Link, NavLink } from 'react-router-dom';

const NavBar = ({ user }) => {
    return (
        <nav className="navbar navbar-expand-md navbar-dark bg-dark">
            <Link to="/" className="navbar-brand">
                Vidly
            </Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div className="navbar-nav">
                    <NavLink to="/movies" className="nav-item nav-link">
                        Movies <span className="sr-only"></span>
                    </NavLink>
                    <NavLink to="/customers" className="nav-item nav-link">
                        Customers <span className="sr-only"></span>
                    </NavLink>
                    <NavLink to="/rentals" className="nav-item nav-link">
                        Rentals <span className="sr-only"></span>
                    </NavLink>

                    {!user && (
                        <React.Fragment>
                            <NavLink to="/login" className="nav-item nav-link">
                                Login <span className="sr-only"></span>
                            </NavLink>
                            <NavLink to="/register" className="nav-item nav-link">
                                Register <span className="sr-only"></span>
                            </NavLink>
                        </React.Fragment>
                    )}
                    {user && (
                        <React.Fragment>
                            <NavLink to="/profile" className="nav-item nav-link">
                                {user.name} <span className="sr-only"></span>
                            </NavLink>
                            <NavLink to="/logout" className="nav-item nav-link">
                                Logout <span className="sr-only"></span>
                            </NavLink>
                        </React.Fragment>
                    )}
                </div>
            </div>
        </nav>
    )
}

export default NavBar;