import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { getMovies, deleteMovie } from "../services/movieService";
import { getGenres } from '../services/genreService';
import Pagination from "./common/pagination";
import { paginate } from "../utils/paginate";
import ListGroup from "./common/listGroup";
import MoviesTable from "./moviesTable";
import _ from 'lodash';
import SearchBox from "./common/searchBox";
import { toast } from "react-toastify";

class Movies extends Component {
    state = {
        movies: [],
        genres: [],
        pageSize: 4,
        currentPage: 1,
        searchQuery: '',
        selectedGenre: null,
        sortColumn: { path: 'title', order: 'asc' }
    };

    async componentDidMount() {
        const { data } = await getGenres();
        const genres = [{ _id: '', name: 'All Genres', }, ...data];

        const { data: movies } = await getMovies();
        this.setState({ movies, genres });
    }

    handleLike = (movie) => {
        const movies = [...this.state.movies];
        const index = movies.indexOf(movie);
        movies[index] = { ...movies[index] };
        movies[index].liked = !movies[index].liked;

        this.setState({ movies });
    };
    handleDelete = async (movie) => {
        const originalMovies = this.state.movies;
        const movies = originalMovies.filter((m) => m._id !== movie._id);
        this.setState({ movies });

        try {
            await deleteMovie(movie._id);
        } catch (error) {
            if (error.response && error.response.status === 404) {
                toast.error('This movie has already been deleted.');
            }

            this.setState({ movies: originalMovies });
        }
    };
    handlePageChange = (page) => {
        this.setState({ currentPage: page });
    };
    handleGenreSelect = (genre) => {
        this.setState({
            selectedGenre: genre,
            searchQuery: '',
            currentPage: 1
        });
    }
    handleSort = (sortColumn) => {
        this.setState({ sortColumn });
    };
    handleSearch = (query) => {
        this.setState({
            searchQuery: query,
            selectedGenre: null,
            currentPage: 1
        });
    }

    getPagedData = () => {
        const { pageSize, currentPage, selectedGenre, searchQuery, movies: allMovies, sortColumn } = this.state;

        // Filtering
        let filteredMovies = allMovies;
        if (searchQuery) {
            filteredMovies = allMovies.filter(m => m.title.toLowerCase().startsWith(searchQuery.toLowerCase()));
        }
        else if (selectedGenre && selectedGenre._id) {
            filteredMovies = allMovies.filter(m => m.genre._id === selectedGenre._id);
        }

        // Sorting
        const sortedMovies = _.orderBy(filteredMovies, [sortColumn.path], [sortColumn.order]);

        // Pagination
        const movies = paginate(sortedMovies, currentPage, pageSize);

        return { totalCount: filteredMovies.length, data: movies }
    }

    render() {
        const { length: moviesCount } = this.state.movies;
        const { pageSize, currentPage, sortColumn, searchQuery } = this.state;
        const { user } = this.props;

        if (moviesCount === 0) return <p>There are no movies in the database.</p>;

        const { totalCount, data: movies } = this.getPagedData();

        return (
            <div className="row">
                <div className="col-3">
                    <ListGroup
                        items={this.state.genres}
                        selectedItem={this.state.selectedGenre}
                        onItemSelect={this.handleGenreSelect}
                    ></ListGroup>
                </div>
                <div className="col">
                    {user && (
                        <Link
                            to="/movies/new"
                            className="btn btn-primary"
                            style={{ marginBottom: 20 }}
                        >
                            New Movie
                        </Link>
                    )}
                    <p>Showing {totalCount} movies in the database.</p>
                    <SearchBox value={searchQuery} onChange={this.handleSearch} />
                    <MoviesTable
                        movies={movies}
                        sortColumn={sortColumn}
                        onLike={this.handleLike}
                        onDelete={this.handleDelete}
                        onSort={this.handleSort}
                    ></MoviesTable>
                    <Pagination
                        itemsCount={totalCount}
                        pageSize={pageSize}
                        currentPage={currentPage}
                        onPageChange={this.handlePageChange}
                    ></Pagination>
                </div>
            </div>
        );
    }
}

export default Movies;
